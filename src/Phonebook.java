import java.util.ArrayList;

public class Phonebook {

    public ArrayList<Contact> contacts = new ArrayList<>();

    // constructor
    public Phonebook() {
        contacts = new ArrayList<Contact>();
    }
//    public Phonebook(ArrayList<Contact> contacts) {
//        this.contacts = contacts;
//    }

    // getter and setter
    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    public void setContacts(Contact contact) {
        contacts.add(contact);
    }

    // methods
}

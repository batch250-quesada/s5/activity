
public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("Hye-kyo", "12345-000000", "Skina Japan, Cebu City, Cebu");
        Contact contact2 = new Contact("So-hee", "56789-000000", "Skina Germany, Cebu City, Cebu");
        Contact contact3 = new Contact("IU", null, "Skina Germany, Cebu City, Cebu");
        Contact contact4 = new Contact("Baby", "43215-000000", null);

//        phonebook.contacts.add(contact1);
//        phonebook.contacts.add(contact2);
        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);
        phonebook.setContacts(contact3);
        phonebook.setContacts(contact4);

        System.out.println("\nPHONEBOOK (" + phonebook.contacts.size() + ")\n");
        System.out.printf("%-10s%20s%13sAddress\n", "Name", "Phone Number", " ");
        System.out.println();

        // number 12 to 14
        if(phonebook.contacts.isEmpty()) System.out.println("(0) contacts");
        else {
            for (Contact contact: phonebook.contacts){
                if (contact.getAddress() != null && contact.getContactNumber() != null) {
                  System.out.printf("%-10s%8s%-15s%10s%-40s\n", contact.getName(), " ", contact.getContactNumber(), " ", contact.getAddress());
                }

                else if (contact.getContactNumber() == null ) {
                    System.out.printf("%-10s%8s%-15s%10s%-40s\n", contact.getName(), " ", "Not registered", " ", contact.getAddress());
                }

                else if (contact.getAddress() == null ) {
                    System.out.printf("%-10s%8s%-15s%10s%-40s\n", contact.getName(), " ", contact.getContactNumber(), " ", "Not registered");
                }
            }
        }
    }
}